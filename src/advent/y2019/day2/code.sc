val expected = 19690720
var result = 0
val pairs = (0 to 99).flatMap(n => (0 to 99).map(v => (n, v))).toIterator
while (result != expected) {
  val (noun, verb) = pairs.next()
  val input = io.Source.fromFile("/Users/jean/dev/advent/src/advent/y2019/day2/input").getLines()
    .flatMap(_.split(",").map(_.toInt)).toList.toBuffer
  input(1) = noun
  input(2) = verb
  var i = 0
  while (input(i) != 99) {
    input(i) match {
      case 1 => input(input(i + 3)) = input(input(i + 1)) + input(input(i + 2))
        i += 4
      case 2 => input(input(i + 3)) = input(input(i + 1)) * input(input(i + 2))
        i += 4
    }
  }
  result=input(0)
  if(result==expected){
  println(result)
  println(noun)
  println(verb)
  }
}

