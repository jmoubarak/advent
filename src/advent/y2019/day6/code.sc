val input = io.Source.fromFile("/Users/jean/dev/advent/src/y2019/day6/input.txt").getLines().toList
  .map(_.split("\\)")).map(x => x(1) -> x(0)).toMap

def getChain(o: String): Seq[String] = {
  input.get(o) match {
    case None => Seq()
    case Some(v) if v == "COM" => Seq(v)
    case Some(v) => v+:getChain(v)
  }
}
//input.keySet

//val x = input.head._1
//getChain(x).foreach(println)

val chains = input.keySet.toList.map { obj => obj-> getChain(obj) }.toMap
//x.map(_.size).sum

val you = chains("YOU")
val san = chains("SAN")

val inter = you.intersect(san)
you.indexOf(inter.head) + san.indexOf(inter.head)
