package advent.y2019.day14

import advent.y2019.Y19Day

import scala.annotation.tailrec
import scala.collection.mutable

object Day14 extends Y19Day(14) {

  case class Reaction(ingredients: Map[String, Int],
                      result: String,
                      resultQty: Int)

  val reactions = input
    .map(i => i.split("=>").toList)
    .map {
      case ingredients :: results :: _ =>
        val ingMap = ingredients.trim
          .split(",")
          .map(_.trim.split(" "))
          .map(x => x(1) -> x(0).toInt)
          .toMap
        val Array(qty, res) = results.trim.split(" ")
        Reaction(ingMap, res, qty.toInt)
    }

//  reactions.take(5).foreach(println)

  val reactionMap = reactions.groupBy(_.result).mapValues(_.head)

  val elementPool =
    mutable.HashMap[String, Long](reactionMap.keySet.map(_ -> 0L).toSeq: _*)

  var totalOre: Long = 0L

  def fuel(ing: String, requested: Long): Long = {
//    println(s"request for $requested $ing")

    val reaction = reactionMap(ing)

    val reactionCoef = (requested.toDouble / reaction.resultQty).ceil.toLong
    val output = reactionCoef * reaction.resultQty

//    println(s"make $output $ing")

    reaction.ingredients.foreach {
      case ("ORE", n) =>
        val newOre = n * reactionCoef
//        println(s"Adding $newOre ORE")
        totalOre += newOre
      case (i, ni) =>
        val existing = elementPool(i)
        val required = (reactionCoef * ni) - existing
        val actual = fuel(i, required)
        elementPool(i) = actual - required
    }

    output

  }

  def resetAndFuel(requested: Long): Long = {
    totalOre = 0
    elementPool.keySet.foreach(elementPool(_) = 0)
    fuel("FUEL", requested)
    totalOre
  }

  val part1 = resetAndFuel(1)
  println(s"part 1: $part1")

  @tailrec
  def binarySearch(x: Long, y: Long): ((Long, Long), (Long, Long)) = {
//    println(s"$x $y")
    if ((y - x) > 1) {
      val mid = x + (y - x) / 2
      if (resetAndFuel(mid) <= 1e12) {
        binarySearch(mid, y)
      } else {
        binarySearch(x, mid)
      }
    } else {
      (x -> resetAndFuel(x), y -> resetAndFuel(y))
    }
  }

  val part2 = binarySearch(1e5.toLong, 1e8.toLong)
  println(s"part2: $part2")
}
