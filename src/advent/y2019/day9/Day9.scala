package advent.y2019.day9

import advent.y2019.{IntComputer, Y19Day}

object Day9 extends Y19Day(9){

  val program = input.flatMap(_.split(",").map(_.toLong))
  val computer = new IntComputer(program, false)

  println(IntComputer(program).runWithInputs(Iterator(1), false))
  println(IntComputer(program).runWithInputs(Iterator(2), true))

}
