val input = io.Source.fromFile("/Users/jean/dev/advent/src/advent/y2019/day8/input.txt").getLines()
  .toList.head.map(_.toString.toInt)


val width = 25
val height = 6
val pixelsPerLayer = width*height
val nLayers = input.length / pixelsPerLayer

val layers = input.grouped(pixelsPerLayer).toList

val min0 = layers.minBy(_.count(_==0))
val part1 = min0.count(_==1) * min0.count(_==2)

val layers2d = layers.map(l=>l.grouped(width).toArray.map(_.toArray))

val image = Array.fill(height, width)(2)
image


def printImage(image:Array[Array[Int]]):Unit={
  val im = image.map(l=>l.map{
    case 1 => "_"
    case 0 => "█"
    case c:Int => c.toString

  }.mkString("")
  ).mkString("\n")
  println(im)
}

layers2d.foreach{layer=>
  (0 until height).foreach{y=>
    (0 until width).foreach{ x=>
      if(image(y)(x)==2){
        image(y)(x)=layer(y)(x)
      }
    }
  }
}
printImage(image)
