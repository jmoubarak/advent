val w1::w2::Nil = io.Source.fromFile("C:\\Users\\jean_\\dev\\advent\\src\\advent\\y2019\\day3\\input.txt").getLines()
  .toList
    .map(_.split(",").toList)

//val w1 = "R8,U5,L5,D3".split(",").toList
//val w2 = "U7,R6,D4,L4".split(",").toList
//
//val w1 = "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51".split(",").toList
//val w2 = "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7".split(",").toList


def getCoord(w:List[String]):List[(Int,Int)]= {
  w.scanLeft(List[(Int,Int)]((0, 0))) { case(coords, action) =>
    val (x,y) = coords.last
    action.splitAt(1) match {
    case ("R", n) => (1 to n.toInt).map(i=> (x + i, y)).toList
    case ("U", n) => (1 to n.toInt).map(i=>(x, y + i)).toList
    case ("L", n) => (1 to n.toInt).map(i=>(x - i, y)).toList
    case ("D", n) => (1 to n.toInt).map(i=>(x, y - i)).toList
  }
  }.flatten
}
val c1 = getCoord(w1)
val c2 = getCoord(w2)

val intersections = c1.intersect(c2).drop(1)
//intersections.minBy{case(x,y)=>math.abs(x)+math.abs(y)}
//intersections.map{case(x,y)=> math.abs(x)+math.abs(y) }.min

intersections.map{i=> c1.indexOf(i)+c2.indexOf(i)}.min