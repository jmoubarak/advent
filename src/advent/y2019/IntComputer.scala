package advent.y2019

class IntComputer(input: Seq[Long], verbose: Boolean) {

  private val memory = Array.fill[Long](input.length * 10)(0L)
  input.copyToArray(memory)
  private var relativeBase = 0


  def log(str: Any): Unit = {
    if (verbose) println(str)
  }

  def p(v: Long, t: Int): Long = {
    t match {
      case 0 => memory(v.toInt)
      case 1 => v
      case 2 =>
        memory(relativeBase + v.toInt)
      case _ => throw new Exception(s"Unknown parameter type: $t")
    }
  }

  def adr(v: Long, t: Int): Int = {
    t match {
      case 0 => v.toInt
      case 2 => relativeBase + v.toInt
      case _ => throw new Exception(s"Invalid result type $t")
    }
  }

  def paramTypes(instr: String): (Int, Int, Int) = {
    instr.length match {
      case 1 => (0, 0, 0)
      case 2 => throw new Exception("instruction size can't be 2")
      case 3 => (0, 0, instr(0) - 48)
      case 4 => (0, instr(0).toInt - 48, instr(1).toInt - 48)
      case 5 => (instr(0).toInt - 48, instr(1).toInt - 48, instr(2).toInt - 48)
    }
  }


  def op3(instr: String, noun: Long, verb: Long, result: Int)(f: (Long, Long) => Long): Unit = {
    log(s"op3 $instr $noun $verb $result")
    val (resType, verbType, nounType) = paramTypes(instr)
    memory(adr(result, resType)) = f(p(noun, nounType), p(verb, verbType))
  }

  def sum(instr: String, noun: Long, verb: Long, result: Long): Unit = {
    require(instr.takeRight(1) == "1")
    op3(instr, noun, verb, result.toInt)(_ + _)
  }

  def multiply(instr: String, noun: Long, verb: Long, result: Long): Unit = {
    require(instr.takeRight(1) == "2")
    op3(instr, noun, verb, result.toInt)(_ * _)
  }

  def scan(instr: String, v: Long, providedInput: Long): Unit = {
    require(instr.takeRight(1) == "3")
    val (_, _, resultType) = paramTypes(instr)
    val resultAdr = adr(v, resultType)
    log(s"input $providedInput to position $resultAdr")
    memory(resultAdr) = providedInput
  }

  def output(instr: String, source: Long): Long = {
    require(instr.takeRight(1) == "4")
    val (_, _, sourceType) = paramTypes(instr)
    val result = p(source, sourceType)
    log(s"output $result")
    result
  }

  def jump(instr: String, condParam: Long, value: Long)(cond: Long => Boolean): Option[Int] = {
    val (_, vt, ct) = paramTypes(instr)
    val c = p(condParam, ct)
    if (cond(c)) {
      val dest = p(value, vt)
      log(s"jump to $dest")
      Some(dest.toInt)
    } else {
      None
    }
  }

  def jumpIfTrue(instr: String, cond: Long, value: Long): Option[Int] = {
    log(s"jump if true $instr $cond $value")
    require(instr.takeRight(1) == "5")
    jump(instr, cond, value)(_ > 0)
  }

  def jumpIfFalse(instr: String, cond: Long, value: Long): Option[Int] = {
    log(s"jump if false $instr $cond $value")
    require(instr.takeRight(1) == "6")
    jump(instr, cond, value)(_ == 0)
  }

  def lessThan(instr: String, noun: Long, verb: Long, result: Int): Unit = {
    require(instr.takeRight(1) == "7")

    op3(instr, noun, verb, result) { (a, b) =>
      if (a < b) 1 else 0
    }
  }

  def equalsOp(instr: String, noun: Long, verb: Long, result: Int): Unit = {
    require(instr.takeRight(1) == "8")

    op3(instr, noun, verb, result) { (a, b) =>
      if (a == b) 1 else 0
    }
  }

  def adjustRelativeBase(instr: String, v: Long): Unit = {
    require(instr.takeRight(1) == "9")
    val (_, _, sourceType) = paramTypes(instr)
    val result = p(v, sourceType)
    log(s"Add ${result.toInt} to relative base.")
    relativeBase += result.toInt
    log(s"new relative base $relativeBase")
  }

  var i = 0

  def runWithInputs(providedInput: Iterator[Long], stopAtFirstOutput: Boolean): Option[Long] = {
    var outputRes: Option[Long] = None
    while (memory(i) != 99 && (!stopAtFirstOutput || outputRes.isEmpty)) {
      log(memory(i))
      memory(i) match {
        case instr if instr.toString.endsWith("1") => sum(instr.toString, memory(i + 1), memory(i + 2), memory(i + 3))
          i += 4
        case instr if instr.toString.endsWith("2") => multiply(instr.toString, memory(i + 1), memory(i + 2), memory(i + 3))
          i += 4
        case instr if instr.toString.endsWith("3") => scan(instr.toString, memory(i + 1), providedInput.next())
          i += 2
        case instr if instr.toString.endsWith("4") => outputRes = Some(output(instr.toString, memory(i + 1)))
          i += 2
        case instr if instr.toString.endsWith("5") => val j = jumpIfTrue(instr.toString, memory(i + 1), memory(i + 2))
          i = j.getOrElse(i + 3)
        case instr if instr.toString.endsWith("6") => val j = jumpIfFalse(instr.toString, memory(i + 1), memory(i + 2))
          i = j.getOrElse(i + 3)
        case instr if instr.toString.endsWith("7") => lessThan(instr.toString, memory(i + 1), memory(i + 2), memory(i + 3).toInt)
          i += 4
        case instr if instr.toString.endsWith("8") => equalsOp(instr.toString, memory(i + 1), memory(i + 2), memory(i + 3).toInt)
          i += 4
        case instr if instr.toString.endsWith("9") => adjustRelativeBase(instr.toString, memory(i + 1))
          i += 2
      }
    }
    outputRes
  }

}

object IntComputer {
  def apply(input: Seq[Long], verbose: Boolean=false): IntComputer = new IntComputer(input, verbose)
}

