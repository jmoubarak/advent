package advent.y2019.day11

import advent.y2019.{IntComputer, Y19Day}

import collection.mutable
import scala.annotation.tailrec

object Day11 extends Y19Day(11){

  val program = input.flatMap(_.split(",").map(_.toLong))

  val grid = mutable.HashMap[(Int,Int),Int]()

  case class Robot(var x:Int, var y:Int){

    sealed trait Direction
    case object Up extends Direction
    case object Left extends Direction
    case object Down extends Direction
    case object Right extends Direction

    var direction: Direction = Up

    def turnLeft():Unit={
      direction match {
        case Up => direction = Left
        case Left => direction = Down
        case Down => direction = Right
        case Right => direction=Up
      }
    }
    def turnRight(): Unit ={
      direction match {
        case Up => direction = Right
        case Left => direction = Up
        case Down => direction = Left
        case Right => direction=Down
      }
    }

    def move():(Int, Int)={
      direction match {
        case Up =>  y+=1
        case Left => x-=1
        case Down => y-=1
        case Right => x+=1
      }
      (x,y)
    }

    val computer = new IntComputer(program,false)

    @tailrec
    final def run():Unit={
      computer.runWithInputs(Iterator(grid.get(x, y).map(_.toLong).getOrElse(0L)), stopAtFirstOutput = true) match {
        case None => println("Program terminated")
        case Some(newColor) if newColor==0 || newColor==1 => println(s"Paint ($x,$y) $newColor")
          grid((x,y))=newColor.toInt
        case Some(value) => throw new Exception(s"Invalid paint output $value")
      }
      computer.runWithInputs(Iterator(), stopAtFirstOutput = true) match {
        case None => println("Program terminated")
        case Some(0) => turnLeft()
          move()
          run()
        case Some(1) => turnRight()
          move()
          run()
        case Some(value) => throw new Exception(s"Invalid direction output $value")
      }

    }

  }

  val robot = Robot(0,0)
  grid((0,0))=1
  robot.run()
  println(grid.keySet.size)

  val xSet = grid.keySet.map(_._1)
  val ySet = grid.keySet.map(_._2)

  (ySet.min to ySet.max).reverse.map{y=>
    (xSet.min to xSet.max).map{x=>
      grid.get((x, y)) match {
        case None => "█"
        case Some(0) => "█"
        case Some(1) => " "
      }
    }.mkString("")
  }.foreach(println)

}
