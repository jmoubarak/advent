 import scala.collection.mutable
 import scala.util.Random

class IntProgram(input:mutable.Buffer[Int], verbose:Boolean) {

  //  val input = io.Source.fromFile("/Users/jean/dev/advent2019/src/day5/input.txt").getLines()
  //    .flatMap(_.split(",").map(_.toInt)).toList.toBuffer
  //val input = """3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99"""
  //  .split(",").map(_.toInt).toBuffer


  def log(str:Any):Unit={
    if(verbose) println(str)
  }

  def p(v: Int, t: Int): Int = {
    if (t == 0) input(v)
    else if (t == 1) v
    else throw new Exception(s"Unknown parameter type: $t")
  }

  def paramTypes(instr: String): (Int, Int, Int) = {
    instr.length match {
      case 1 => (0, 0, 0)
      case 2 => throw new Exception("instruction size can't be 2")
      case 3 => (0, 0, instr(0) - 48)
      case 4 => (0, instr(0).toInt - 48, instr(1).toInt - 48)
      case 5 => (instr(0).toInt - 48, instr(1).toInt - 48, instr(2).toInt - 48)

    }
  }


  def op3(instr: String, noun: Int, verb: Int, result: Int)(f: (Int, Int) => Int): Unit = {
    log(s"op3 $instr $noun $verb $result")
    val (resType, verbType, nounType) = paramTypes(instr)
    require(resType == 0, "result param type must be 0")
    input(result) = f(p(noun, nounType), p(verb, verbType))
  }

  def sum(instr: String, noun: Int, verb: Int, result: Int): Unit = {
    require(instr.takeRight(1) == "1")
    op3(instr: String, noun: Int, verb: Int, result: Int)(_ + _)
  }

  def multiply(instr: String, noun: Int, verb: Int, result: Int): Unit = {
    require(instr.takeRight(1) == "2")
    op3(instr: String, noun: Int, verb: Int, result: Int)(_ * _)
  }

  def jump(instr: String, condParam: Int, value: Int)(cond: Int => Boolean): Option[Int] = {
    val (_, vt, ct) = paramTypes(instr)
    val c = p(condParam, ct)
    if (cond(c)) {
      val dest = p(value, vt)
      log(s"jump to $dest")
      Some(dest)
    } else {
      None
    }
  }

  def jumpIfTrue(instr: String, cond: Int, value: Int): Option[Int] = {
    log(s"jump if true $instr $cond $value")
    require(instr.takeRight(1) == "5")
    jump(instr, cond, value)(_ > 0)
  }

  def jumpIfFalse(instr: String, cond: Int, value: Int): Option[Int] = {
    log(s"jump if false $instr $cond $value")
    require(instr.takeRight(1) == "6")
    jump(instr, cond, value)(_ == 0)
  }

  def lessThan(instr: String, noun: Int, verb: Int, result: Int): Unit = {
    require(instr.takeRight(1) == "7")

    def f(a: Int, b: Int) = if (a < b) 1 else 0

    op3(instr, noun, verb, result)(f)
  }

  def equalsOp(instr: String, noun: Int, verb: Int, result: Int): Unit = {
    require(instr.takeRight(1) == "8")

    def f(a: Int, b: Int) = if (a == b) 1 else 0

    op3(instr, noun, verb, result)(f)
  }

  def scan(instr: String, result: Int, providedInput:Int): Unit = {
    require(instr.takeRight(1) == "3")
    //  require(instr.dropRight(2) == "0", "result param type must be 0")
    log(s"input $providedInput to position $result")
    input(result) = providedInput
  }

  def output(instr: String, source: Int): Int = {
    require(instr.takeRight(1) == "4")
    val (_, _, sourceType) = paramTypes(instr)

    val result = if (sourceType == 0) {
      input(source)
    } else if (sourceType == 1) {
      source
    } else {
      throw new Exception(s"Unknown parameter type: $sourceType")
    }
    log(s"output $result")
    result
  }

  var i = 0
  def runWithInputs(providedInput:Iterator[Int]):Option[Int]= {
    var outputRes:Option[Int] = None
    while (input(i) != 99 && outputRes.isEmpty) {
      log(input(i))
      input(i) match {
        case instr if instr.toString.endsWith("1") => sum(instr.toString, input(i + 1), input(i + 2), input(i + 3))
          i += 4
        case instr if instr.toString.endsWith("2") => multiply(instr.toString, input(i + 1), input(i + 2), input(i + 3))
          i += 4
        case instr if instr.toString.endsWith("3") => scan(instr.toString, input(i + 1), providedInput.next())
          i += 2
        case instr if instr.toString.endsWith("4") => outputRes=Some(output(instr.toString, input(i + 1)))
          i += 2
        case instr if instr.toString.endsWith("5") => val j = jumpIfTrue(instr.toString, input(i + 1), input(i + 2))
          i = j.getOrElse(i + 3)
        case instr if instr.toString.endsWith("6") => val j = jumpIfFalse(instr.toString, input(i + 1), input(i + 2))
          i = j.getOrElse(i + 3)
        case instr if instr.toString.endsWith("7") => lessThan(instr.toString, input(i + 1), input(i + 2), input(i + 3))
          i += 4
        case instr if instr.toString.endsWith("8") => equalsOp(instr.toString, input(i + 1), input(i + 2), input(i + 3))
          i += 4
      }
    }
    outputRes
  }


}


val input = io.Source.fromFile("/Users/jean/dev/advent/src/advent/y2019/day7/input.txt").getLines()
  .flatMap(_.split(",").map(_.toInt)).toList.toBuffer

// val phases = Seq(0,1,2,3,4)
// val allPhases = phases.permutations
// allPhases
//
// val allThrusts = allPhases.map{combi =>
//  combi ->combi.foldLeft(0){
//    case(res,p) => new IntProgram(input.clone()).runWithInputs(Iterator(p,res))
//      .getOrElse(throw new Exception(s"no output was returned for input $res and phase $p in phases $combi") )
//  }
// }
// allThrusts.maxBy(_._2)

 val phases = Seq(5,6,7,8,9)
 val allPhases = phases.permutations
  allPhases

val allThrusts = allPhases.map { combi =>
   val programs = combi.map(p => p -> new IntProgram(input.clone(), false)).toMap
   val firstRun = combi.foldLeft(0) { case (res, ph) => programs(ph).runWithInputs(Iterator(ph, res))
     .getOrElse(throw new Exception(s"no output was returned for input $res and phase $ph in phases $combi"))
   }
   var th = firstRun
   var i = 0
   try {
     while (true) {
       th = combi.foldLeft(th) { case (res, ph) => programs(ph).runWithInputs(Iterator(res))
         .getOrElse(throw new Exception(s"no output was returned for input $res and phase $ph in phases $combi"))
       }
       i += 1
     }
   } catch {
     case ex: Exception =>
       println(ex.getMessage)
       println(s"Amplification ended after $i loops. final value: $th")
   }
  combi-> th
 }
 allThrusts.maxBy(_._2)