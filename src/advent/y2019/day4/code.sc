val min = 231832
val max = 767346

val candidates = (min to max).filter{
  i=>
    val str = i.toString
    str.sliding(2).exists(couple=>couple.take(1) == couple.takeRight(1)) &&
    str.sorted == str
}

candidates.size

val candiates2= candidates.filter{c=>
  val str = c.toString
  str.groupBy(identity).exists{case(_,cand)=> cand.length==2}
}
candiates2
candiates2.length