package advent.y2019.day13

import advent.y2019.IntComputer
import advent.y2019.Y19Day

import scala.collection.mutable

object Day13 extends Y19Day(13) {

  val program = input.flatMap(_.split(",").map(_.toLong))
  println(program)

  val screen = mutable.HashMap[(Int, Int), Int]()

  def part1(): Int = {
    val computer = IntComputer(program)
    Iterator
      .continually(computer.runWithInputs(Iterator(), true))
      .takeWhile(output => output.isDefined)
      .grouped(3)
      .foreach {
        case Some(x) :: Some(y) :: Some(id) :: Nil =>
          screen((x.toInt, y.toInt)) = id.toInt
        case x => throw new IllegalStateException(s"output tuple can't be $x")
      }
    screen.count(_._2 == 2)
  }

  def part2(): Int = {
    val computer = IntComputer(2L +: program.drop(1))
    var score = 0
    def getInput(xb: Int, xp: Int): Int = {
      if (xb < xp) {
        -1
      } else if (xb > xp) {
        1
      } else {
        0
      }
    }
    val paddle = screen.filter(_._2 == 3).head._1
    var xPaddle = paddle._1
    println(paddle)
    val ball = screen.filter(_._2 == 4).head._1
    val xBall0 = ball._1
    println(ball)
    var nextInput = getInput(xBall0, xPaddle)
    Iterator
      .continually(computer.runWithInputs(Iterator(nextInput), true))
      .takeWhile(output => output.isDefined)
      .grouped(3)
      .foreach {
        case Some(-1L) :: Some(0L) :: Some(s) :: Nil =>
          score = s.toInt
        case Some(x) :: Some(y) :: Some(4) :: Nil =>
          nextInput = getInput(x.toInt, xPaddle)
        case Some(x) :: Some(y) :: Some(3) :: Nil =>
          xPaddle = x.toInt
        case Some(x) :: Some(y) :: Some(id) :: Nil =>
            screen((x.toInt, y.toInt)) = id.toInt
        case x => throw new IllegalStateException(s"output tuple can't be $x")
      }
    score
  }

  val res1 = part1()
  println(s"num blocks: $res1")
  val res2 = part2()
  println(s"final score $res2")
  println(screen.count(_._2 == 2))

}
