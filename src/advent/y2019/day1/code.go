package main

import (
	"bufio"
	"math"
	"os"
	"strconv"
)

func main() {
	file, _ := os.Open("/home/jean/dev/advent/src/advent.y2019/day1/input.txt")
	scanner := bufio.NewScanner(file)
	input := make([]int64, 1000)
	for scanner.Scan() {
		n, _ := strconv.ParseInt(scanner.Text(), 10, 64)
		input = append(input, n)
	}
	// for index := 0; index < len(input); index++ {

	// 	println(input[index])
	// }
	var totalFuel = int64(0)
	for index := 0; index < len(input); index++ {
		moduleTotal := int64(0)
		newFuel := getFuel(input[index])

		for newFuel > 0 {
			moduleTotal+= newFuel
			newFuel = getFuel(newFuel)
		}
		totalFuel += moduleTotal
	}

	println(totalFuel)

}

func getFuel(mass int64) int64 {
	return int64(math.Floor(float64(mass)/3)) - 2
}
