
with open("/home/jean/dev/advent/src/advent.y2019/day1/input.txt",'r') as f:
    input = [int(n) for n in f]

input

fuel = [i//3 -2 for i in input]
sum(fuel)

def getFuel(mass):
    return mass//3 -2

total_fuels=[]

for i in input:
    total = 0
    add_fuel = getFuel(i)
    while add_fuel>0:
        total+=add_fuel
        add_fuel=getFuel(add_fuel)
    total_fuels.append(total)

sum(total_fuels)