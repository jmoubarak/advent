package advent.y2019

import advent.Day

abstract class Y19Day(val day:Int) extends Day(2019, day)