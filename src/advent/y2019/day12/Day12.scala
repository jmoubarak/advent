package advent.y2019.day12

import advent.y2019.Y19Day

import scala.collection.mutable

object Day12 extends Y19Day(12) {

  case class Moon(var x: Int,
                  var y: Int,
                  var z: Int,
                  var vx: Int,
                  var vy: Int,
                  var vz: Int) {

    def update(): Unit = {
      x += vx
      y += vy
      z += vz
    }
    def dim(d: Int): (Int, Int) = {
      d match {
        case 1 => (x, vx)
        case 2 => (y, vy)
        case 3 => (z, vz)
      }
    }

    def totalEnergy: Int =
      Seq(x, y, z).map(math.abs).sum * Seq(vx, vy, vz).map(math.abs).sum

    def hashCode(dim: Int): Int = {
      dim match {
        case 1 => copy(x, 0, 0, vx, 0, 0).hashCode()
        case 2 => copy(0, y, 0, 0, vy, 0).hashCode()
        case 3 => copy(0, 0, z, 0, 0, vz).hashCode()
      }
    }
  }

  val moons = input
    .map(_.drop(1).dropRight(1).split(",").map(_.split("=")(1).toInt))
    .map(c => Moon(c(0), c(1), c(2), 0, 0, 0))

  moons.foreach(println)

  def applyGravity(m1: Moon, m2: Moon): Unit = {
    def applyGravity(c1: Int, c2: Int): (Int, Int) = {
      if (c1 < c2) {
        (1, -1)
      } else {
        if (c1 > c2) {
          (-1, 1)
        } else {
          (0, 0)
        }
      }
    }

    val (d1x, d2x) = applyGravity(m1.x, m2.x)
    val (d1y, d2y) = applyGravity(m1.y, m2.y)
    val (d1z, d2z) = applyGravity(m1.z, m2.z)

    m1.vx += d1x
    m2.vx += d2x
    m1.vy += d1y
    m2.vy += d2y
    m1.vz += d1z
    m2.vz += d2z
  }

  val combinations = moons.zipWithIndex.flatMap {
    case (m1, i) => moons.drop(i + 1).map(m2 => (m1, m2))
  }

  val steps = (1 to 3)
    .map { dim =>
      val history = mutable.HashSet[Seq[(Int, Int)]]()
      var i = 0
      while (!history.contains(moons.map(_.dim(dim)))) {
        history.add(moons.map(_.dim(dim)))
        combinations.foreach { case (m1, m2) => applyGravity(m1, m2) }
        moons.foreach(_.update())
        i += 1
      }
      println(s"dim $dim: $i")
      i.toLong
    }

  println(steps)

  def gcd(a: Long, b: Long): Long = if (b == 0) a.abs else gcd(b, a % b)
  def lcm(a: Long, b: Long): Long = a * b / gcd(a, b)
  val result = steps.sorted.reduce(lcm)
  println(result)
}
