val text = io.Source.fromFile("/Users/jean/dev/advent2018/src/day5/input").mkString("")
text.size
//val reduced = text.grouped(2).flatMap{x => if(x(0) != x(1) &&  (x(0).toUpper==x(1) || x(1).toUpper==x(0))) None else Some(s"$x") }.mkString("")

//println(reduced)
//reduced.size
//text.takeRight(5)

def reduce(t: String): String = {
  t.grouped(2).filterNot { x => x.length == 2 && x(0) != x(1) && (x(0).toUpper == x(1) || x(1).toUpper == x(0)) }.mkString("")
}

//val r1 = reduce(text).mkString("")
//r1.length
//val r2 = reduce(r1.drop(1)).mkString("")
//r2.length
//val r3 = reduce(r2.drop(1)).mkString("")
//r3.length
//val r4 = reduce(r3.drop(1)).mkString("")
//r4.length

def shiftReduce(t: String): String = {
  println(t.length)
  val r1 = reduce(t)
  if (r1.length == t.length) {
    val r2 = reduce(t.drop(1))
    if (r2.length == r1.length-1) {
      r1
    } else {
      shiftReduce(r2)
    }
  } else {
    shiftReduce(r1)
  }
}

val reduced = shiftReduce(text)
reduced.length
reduced.takeRight(5)

reduce(reduced.drop(1)).length
println(reduced)

reduced.sliding(2).filter{x => (x(0) != x(1) && (x(0).toUpper == x(1) || x(1).toUpper == x(0)))}

  res5.toList
//def recReduce(t: String): String = {
//  println(t.length)
//  reduce(t) match {
//    case reduced if reduced.length < t.length - 1 => recReduce(reduced)
//    case reduced if reduced.length == t.length => println(s"not reduced: ${reduced.length}")
//      reduce(reduced.drop(1)) match {
//      case reduced2 if reduced2.length == reduced.length - 1 => reduced
//      case reduced2 if reduced2.length < reduced.length - 1 => recReduce(reduced2)
//      case a => println(s"error2:${a.length}")
//        throw new MatchError(a)
//    }
//    case a => println(s"error1:${a.length}")
//      throw new MatchError(a)
//  }
//}
//
//recReduce(text)
