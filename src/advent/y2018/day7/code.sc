import scala.collection.mutable.ListBuffer

val input = io.Source.fromFile("/Users/jean/dev/advent2018/src/day7/input.txt").getLines().toList

val steps = input.map(_.split(" ")).map(arr=>arr(7)->arr(1))
  .groupBy(_._1)
  .mapValues(_.map(_._2))

steps.foreach(println)

val noDeps = ('A' to 'Z').map(_.toString).filterNot(steps.keySet.contains)



val remainingSteps = collection.mutable.HashMap(steps.toSeq:_*)
val toRemove = ListBuffer[String]()
toRemove.appendAll(noDeps)

val completed = ListBuffer[String]()

println(s"starting toRemove: ${toRemove.mkString(",")}")
  while(toRemove.nonEmpty) {
    val currentToRemove = toRemove.sorted.take(5)
    println(s"currentToRemove: $currentToRemove")
    remainingSteps.foreach{case (k,v)=> remainingSteps(k)=v.diff(currentToRemove)}
    val newCompleted = remainingSteps.filter{case(k,v)=> v.isEmpty}.keySet.toList.sorted
    println(s"newCompleted: ${newCompleted.mkString(",")}")
    toRemove.appendAll(newCompleted)
    toRemove.remove(toRemove.indexOf(currentToRemove))
    println(toRemove.mkString(","))
    completed.append(currentToRemove)
    newCompleted.foreach(remainingSteps.remove)
  }
//  println("completed: " + newCompleted.mkString(""))


completed.mkString("")
completed.size