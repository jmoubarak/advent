case class Coordinate(x:Int, y:Int)

val coordinates = io.Source.fromFile("C:\\Users\\jean_\\dev\\advent\\src\\advent\\advent.y2018\\day6\\input.txt").getLines().map(_.split(","))
.map(c=>Coordinate(c(0).trim.toInt,c(1).trim.toInt))
.toList

val xMin = coordinates.map(_.x).min
val xMax= coordinates.map(_.x).max
val yMin = coordinates.map(_.y).min
val yMax = coordinates.map(_.y).max

val grid = (xMin to xMax).flatMap(x=>(yMin to yMax).map(y=> (x,y)))

coordinates.size

def dist(c:Coordinate, x:Int, y:Int)=math.abs(c.x-x)+math.abs(c.y-y)

val distances = grid.map{case p@(x,y)=>p->coordinates.map(c=>c->dist(c,x,y)).toMap}.toMap
val minDistances = distances.mapValues(_.minBy(_._2))
// remove locations with equal min distances
val fDistances = distances.filter{case(point, distMap)=> distMap.count(_==minDistances(point))==1}.mapValues(_.minBy(_._2))

// remove coordinates with infinite areas
val fCoord = fDistances.groupBy(_._2._1).filterNot(_._2.keySet.exists{case(x,y)=>x==xMax || x==xMin || y==yMax || y==yMin})
val areas = fCoord.mapValues(_.size)

areas.toSeq.sortBy(-_._2).take(10)

val closeDistances = distances.filter{case (point,distMap)=> distMap.values.sum < 10000}

val selectedPoints = closeDistances.keySet
selectedPoints.size
