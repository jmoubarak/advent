import scala.collection.mutable.ListBuffer

val raw = io.Source.fromFile("/Users/jean/dev/advent2018/src/day4/input").getLines().toList.sorted

raw.count(_.contains("Guard"))
val guards = ListBuffer[ListBuffer[String]]()


val Wake = "wake"
val Sleep = "sleep"

case class Guard(entries: List[String]) {
  val id = entries.head.split(" ")(3)
  val events = entries.drop(1).map { e =>
    val splits = e.split(" ")
    val minute = splits(1).dropRight(1).split(":").last.toInt
    val action = if (e.contains("begins") || e.contains("wakes")) Wake else Sleep
    minute -> action
  }
  val sleeping = events.map(_._1).grouped(2).map{m => (m(0) until m(1)).toList}.toList.flatten
}

//guards

raw.foreach { r =>
  if (r.contains("Guard")) {
    guards.append(ListBuffer(r))
  } else {
    guards.last.append(r)
  }
}
//val topGuard = guards.map(g => g.head.split(" ")(3) -> g).groupBy(_._1).maxBy(x => x._2.flatMap(_._2).size) // 1097

//guards.map(g => g.head.split(" ")(3) -> g).groupBy(_._1).filterKeys(_ == "#1097").values


val fGuards = guards.map(e=>Guard(e.toList))
val topGard = fGuards.groupBy(_.id).maxBy(_._2.map(_.sleeping.size).sum)._1

fGuards.filter(_.id == topGard).map(_.sleeping).flatten.toList.groupBy(identity).mapValues(_.size).maxBy(_._2)
//fGuards.filter(_.id == "#2251")
//fGuards.filter(_.id == "#2251").map(_.sleeping)

def sleepingCounts(minute:Int)={
  fGuards.groupBy(_.id).mapValues(_.count(_.sleeping.contains(minute))).toMap
}
val counts = (0 to 59).map(m=>m->sleepingCounts(m)).toMap
  val topGuardPerMinute=counts.mapValues(_.maxBy(_._2))
topGuardPerMinute.maxBy(_._2._2)