case class Claim(id:Int, x:Int, y:Int, w:Int, h:Int)

val rawInput = io.Source.fromFile("C:\\Users\\jean_\\dev\\advent\\src\\data\\1").getLines().toList

rawInput.head

val claims = rawInput.map{_.drop(1).split(" |@|,|:|x").filter(_.nonEmpty).map(_.toInt)}.map(a=>Claim(a(0),a(1),a(2),a(3),a(4)))


def cellsOfClaim(c:Claim)=(c.x until (c.x+c.w)).flatMap(j=> (c.y until (c.y + c.h)).map(i=> (i,j)))
val cells = claims.flatMap(c=> cellsOfClaim(c))
val noOverlap = cells.groupBy(identity).filter(_._2.size ==1).keySet.toSeq
claims.map(c=>c.id->cellsOfClaim(c)).dropWhile(_._2.diff(noOverlap).nonEmpty)