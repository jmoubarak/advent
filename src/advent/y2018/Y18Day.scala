package advent.y2018

import advent.Day

abstract class Y18Day(val day:Int) extends Day(2018, day)