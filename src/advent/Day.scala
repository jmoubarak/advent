package advent

abstract class Day(year:Int, day:Int) extends App{
  val inputFile = s"src/advent/y$year/day$day/input.txt"

  lazy val input:List[String] = io.Source.fromFile(inputFile).getLines().toList
}
