import scala.annotation.tailrec

val input = io.Source.fromFile("C:/Users/jean_/dev/advent/src/advent/y2021/day3/input.txt").getLines.toList

val numBits = input.head.length
val columns = (0 until numBits).map(i=> input.map(_(i)).mkString)

val counts = columns.map(_.groupBy(identity).mapValues(_.length))
println(counts)
val gammaString = counts.map(m=> m.maxBy(_._2)._1).mkString
val epsilonString = counts.map(m=> m.minBy(_._2)._1).mkString

val gamma = Integer.parseInt(gammaString, 2)
val epsilon = Integer.parseInt(epsilonString, 2)
val part1 = gamma * epsilon
println(part1)

@tailrec
def getValue(columns: IndexedSeq[String], index: Int, orderingF: Map[Char,Int] => Char):Int={
  columns.head.length match {
    case 1  => Integer.parseInt(columns.map(_.head).mkString,2)
    case x if index == 12 =>
      println(columns)
      0
    case x if x > 1 =>
      val counts = columns.map(_.groupBy(identity).mapValues(_.length)).apply(index)
      val bit = orderingF(counts)
      val remaining = columns.map(col => col.zipWithIndex.filter{case(_, i)=>columns(index)(i) == bit}.map(_._1).mkString)
      println(columns.head.size)
      getValue(remaining, index+1, orderingF)
  }
}

val oxygen = getValue(columns, 0, counts => if (counts.getOrElse('1', 0) >= counts.getOrElse('0', 0)) '1' else '0' )
val co2 = getValue(columns, 0, counts => if (counts.getOrElse('0', 0) <= counts.getOrElse('1',0)) '0' else '1' )
val part2 = oxygen * co2
println(part2)