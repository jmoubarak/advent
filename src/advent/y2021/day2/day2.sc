val input = io.Source.fromFile("C:/Users/jean_/dev/advent/src/advent/y2021/day2/input.txt").getLines.toList

case class Instruction(dir: String, value: Int)

class Submarine{

  var horizontal, depth, aim = 0

  def move(instructions: Seq[Instruction])={
    instructions.foreach{
      case Instruction("forward", v) =>
        horizontal+=v
        depth += v*aim
      case Instruction("down", v) => aim += v
      case Instruction("up", v) => aim -= v
    }
    this
  }

}

val instructions = input.map(_.split(' ')).map(x=>Instruction(x(0), x(1).toInt))
val sub = new Submarine
sub.move(instructions)
val result = sub.horizontal * sub.depth
println(result)
