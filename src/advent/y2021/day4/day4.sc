import scala.collection.mutable
import scala.collection.mutable.ListBuffer

val input = io.Source.fromFile("C:/Users/jean_/dev/advent/src/y2021/advent/day4/input.txt").getLines.toList

val draw = input.head.split(',').map(_.toInt)

case class BingoNumber(value: Int, i:Int, j:Int, drawn:Boolean)

case class BingoBoard(numbers: mutable.Buffer[BingoNumber]){

  def update(draw:Int)={
    val x = numbers.indexWhere(_.value==draw)
    if(x > -1){
      numbers(x) = numbers(x).copy(drawn=true)
    }
  }

  def check()={
    val drawn = numbers.filter(_.drawn)
    drawn.groupBy(_.i).find(_._2.size >=5 )
      .orElse( drawn.groupBy(_.j).find(_._2.size >=5 ))
      .isDefined
  }

}

val boards = input.drop(1).filterNot(_=="").grouped(5)
  .map(boardLines => boardLines.zipWithIndex.flatMap{case(line, i) => line.trim.split(" ").filter(_.nonEmpty).zipWithIndex.map{case(n, j) => BingoNumber(n.toInt, i, j, drawn=false)}}.toBuffer)
  .map(BingoBoard).toList

val boardBuffer = ListBuffer(boards:_*)
val d = draw.find{d=>
  boardBuffer.filter { board =>
    board.update(d)
    board.check()
  }.foreach { board =>
    println(boardBuffer.head.numbers.filterNot(_.drawn).map(_.value).sum * d)

    boardBuffer.remove(boardBuffer.indexOf(board))
    println(d)
  }
  boardBuffer.isEmpty
}
println(d.get)

