val input = io.Source.fromFile("C:/Users/jean_/dev/advent/src/advent/y2021/day1/input.txt").getLines.toList

val part1 = input.map(_.toInt).sliding(2).count{case i::j::Nil => i < j}

println(part1)

// part 2

val part2 = input.map(_.toInt).sliding(3).map(_.sum)
  .sliding(2).count{case i::j::Nil => i < j}
println(part2)